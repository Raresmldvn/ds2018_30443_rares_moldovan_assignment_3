package messaging;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class MessageSender {

	protected Channel channel;
    protected Connection connection;
    protected String queueName;
	
    public MessageSender(String queueName) throws IOException{
         this.queueName = queueName;
		
         //Create a connection factory
         ConnectionFactory factory = new ConnectionFactory();
	    
         //hostname of rabbitmq server
         factory.setHost("localhost");
		
         //getting a connection
         connection = factory.newConnection();
	    
         //creating a channel
         channel = connection.createChannel();
	    
         //declaring a queue for this channel. If queue does not exist,
         //it will be created on the server.
         channel.queueDeclare(queueName, false, false, false, null);
    }
	
    public void sendMessage(Object object) throws IOException {
    	ObjectMapper objectMapper = new ObjectMapper();
	    channel.basicPublish("",queueName, null, objectMapper.writeValueAsString(object).getBytes());
	}	
    
    /**
     * Close channel and connection. Not necessary as it happens implicitly any way. 
     * @throws IOException
     */
     public void close() throws IOException{
         this.channel.close();
         this.connection.close();
     }
}
