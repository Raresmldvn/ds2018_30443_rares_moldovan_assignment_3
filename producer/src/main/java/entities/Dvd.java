package entities;

public class Dvd {

	private String name;
	private int year;
	private double price;
	private String artist;
	public Dvd(String name, int year, double price, String artist) {
		super();
		this.name = name;
		this.year = year;
		this.price = price;
		this.artist = artist;
	}
	
	public Dvd() {}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	
}
