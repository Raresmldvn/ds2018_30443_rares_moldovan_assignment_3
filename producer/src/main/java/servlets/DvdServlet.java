package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entities.Dvd;
import messaging.MessageSender;
import views.DvdView;

public class DvdServlet extends HttpServlet{

	private static final String QUEUE_NAME = "dvdqueue";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		DvdView dvdView = new DvdView();
		String message = request.getParameter("err") != null ?  request.getParameter("err"): "" ;
		String htmlContent = dvdView.getHtmlContent();
		if(message.equals("")) {
			response.getWriter().print(htmlContent);
			return;
		}
		String html = "";
		if(!message.equals("1")) {
			html = htmlContent.replace("<java/>", "You succesfully added the DVD to the store!");
		} else {
			html = htmlContent.replace("<java/>", "Error submitting your DVD!");
		}
		response.getWriter().print(html);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		MessageSender messageSender = new MessageSender(QUEUE_NAME);
		String dvdName = request.getParameter("name");
		Dvd dvdToSend = null;
		try {
		int year = Integer.parseInt(request.getParameter("year"));
		double price = Double.parseDouble(request.getParameter("price"));
		String artist = request.getParameter("artist");
		dvdToSend = new Dvd(dvdName, year, price, artist);
		} catch(Exception e) {
			response.sendRedirect(request.getRequestURL().toString() + "?err=1");
			return;
		}
		messageSender.sendMessage(dvdToSend);
		response.sendRedirect(request.getRequestURL().toString());
	}
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
	}
	@Override
	public void destroy() {
		System.out.println("Servlet " + this.getServletName() + "has been destroyed");
	}
}
