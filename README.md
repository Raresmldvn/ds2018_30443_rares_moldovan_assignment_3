# Dvd store using RabbitMQ 
The following application is a MOM(message orietented middleware) application for a Dvd service application. The main features are:
- Publish dvds.
- Consume dvd objects by sending emails to subscribers or storing the information in a text file.
## Prerequisites
In order to build and execute the application, you need to have the following resources installed:
- JDK version >= 1.8.0
- Maven version >= 1.0.0
- RabbitMQ
- Tomcat version >= 1.8.0

## Build
In order to build the application, you must build the two maven projects: producer and consumer, using Maven install.
```
mvn clean install
```
The producer application is based on Java Servlets therefore it needs to be run on a server. The server is listed in the prerequisites section: Tomcat 1.8.0.
In order to run the producer application, use the following instruction:
If you are building the projects using Eclipse IDE, open 2 different instances and for each of the two maven modules go to Run as -> Maven install.
The emailing service is configured to send mail to only 1 user from a test gmail account. You can go to the Consumer Start class and replace the credentials with your own to
test the application. Moreover, the mails can be sent to any type of email address. In the same place, the Consumer start, replace the current email addresss from the sending mail function
call with the one desired. If you want to send emails to multiple addresses, use a for loop and send for each adress an individual mail by calling the send function.
## Execute
```
In order to execute the producer application run maven with the following instruction (either IDE or terminal_
```
mvn tomcat7:run
```
In order to execute the consumer application, you must run the Consumer Start class.
```
java ClientStart
```
The input form for the DVDs is available at "http://localhost:8080/dvd". Enter the DVD details (after running the consumer application). Log into your account to see if the emails
have been sent succesfully and search the main folder of the consumer application for the text files which persist the DVD data.