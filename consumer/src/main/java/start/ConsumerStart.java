package start;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import entities.Dvd;
import messaging.MessageReceiver;
import services.FileService;
import services.MailService;


public class ConsumerStart {

	private static final String QUEUE_NAME ="dvdqueue";
	
	public static void main(String args[]) throws IOException {
		System.out.println("Waiting for messages!");
		MessageReceiver receiver = new MessageReceiver(QUEUE_NAME);
		final MailService mailService = new MailService("testitestids@gmail.com","test123Te$t");
		final ObjectMapper objectMapper = new ObjectMapper();
		final FileService fileService = new FileService("dvds.txt");
		Consumer consumer = new DefaultConsumer(receiver.getChannel()) {
			  @Override
			  public void handleDelivery(String consumerTag, Envelope envelope,
			                             AMQP.BasicProperties properties, byte[] body)
			      throws IOException {
			    String message = new String(body, "UTF-8");
			    Dvd dvd = objectMapper.readValue(message, Dvd.class);
			    mailService.sendMail("raresmldvn@yahoo.com","New dvd in store: " + dvd.getName(), dvdToMailMessage(dvd));
			    fileService.writeToFile(dvdToFileLine(dvd));
			    System.out.println(" [x] Received '" + message + "'");
			  }
			};
		receiver.startListening(consumer);
	}
	
	private static String dvdToMailMessage(Dvd dvd) {
		String message = "A new dvd was added to our collection!\n";
		message += "Title: " + dvd.getName() + "\n";
		message += "Artist:" + dvd.getArtist() + "\n";
		message += "Year: " + dvd.getYear() + "\n";
		message += "Price: " + dvd.getPrice() + "\n";
		return message;
	}
	
	private static String dvdToFileLine(Dvd dvd) {
		String message = "";
		message += "Title: " + dvd.getName() + ", ";
		message += "Artist:" + dvd.getArtist() + ", ";
		message += "Year: " + dvd.getYear() + ", ";
		message += "Price: " + dvd.getPrice() + "\n";
		return message;
	}
}
