package messaging;

import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;

public class MessageReceiver {

	protected Channel channel;
    protected Connection connection;
    protected String queueName;
    
    public MessageReceiver(String queueName) throws IOException {
    	  this.queueName = queueName;
  		
          //Create a connection factory
          ConnectionFactory factory = new ConnectionFactory();
 	    
          //hostname of your rabbitmq server
          factory.setHost("localhost");
 		
          //getting a connection
          connection = factory.newConnection();
 	    
          //creating a channel
          channel = connection.createChannel();
 	    
          //declaring a queue for this channel. If queue does not exist,
          //it will be created on the server.
          channel.queueDeclare(queueName, false, false, false, null);
    }
    
    public Channel getChannel() {
    	return this.channel;
    }
    public void startListening(Consumer consumer) throws IOException {
    		channel.basicConsume(queueName, true, consumer);
    }
}
