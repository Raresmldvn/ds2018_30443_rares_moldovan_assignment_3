package services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class FileService {

	private String fileName;
	
	public FileService(String fileName) {
		this.fileName = fileName;
	}
	
	public void writeToFile(String content) {
		try {
			PrintWriter out = new PrintWriter(new FileOutputStream(new File(this.fileName), true));
		
			out.println(content);
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
